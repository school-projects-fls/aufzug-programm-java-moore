import javafx.geometry.*;

public class Aufzug_Motor implements Runnable {
  
  //public boolean activeclass = true;
  public boolean driveUp = false;
  public boolean driveDown = false;
  
  public int posY;
  
  public int speed = 1;
  public int delay = 8;
  
  int[] stopPos = {414, 207, 12}; //Limits
  int nextStopID = 0;
  
  public Aufzug_Programm mother;
  
  public Aufzug_Motor(Aufzug_Programm mother, int startPosY) {
    this.mother = mother;
    this.posY = startPosY;
  }
  
  public void run() {
    try {
      while (mother != null) { 
        if(driveUp) posY -= speed;
        if(driveDown) posY += speed;
        mother.elevator.setLayoutY(posY);
        Thread.sleep(delay);
      } // end of while
    } catch(Exception e) {
      e.printStackTrace();
    } 
  }

} // end of class Aufzug_Motor

