/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 16.09.2019
 * @author 
 */

public class Aufzug_Logic implements Runnable {
  
  Aufzug_Programm mother;

  public Aufzug_Logic(Aufzug_Programm mother) {
    this.mother = mother;
  }
  
  public void run() {
    while (mother != null) { 
      mother.ermittleFolgeZustand();
      //System.out.println(mother.zustand);
      
      mother.checkAngekommen();
      
      try {
        Thread.sleep(10);
      } catch(Exception e) {
        e.printStackTrace();
      } 
    } // end of while
  }
  

} // end of class Aufzug_Logic

